# Enigmaticos Python Tools

Python tools I made for myself (and for you if you want to use them)

## Description

This is a bunch of modules I coded for Python. The following modules are included:

· Debug: A smart debugger that you can use to log exceptions and inspect globals and variables during an exception.
· FileHandler: An extension wrapper of Python's file handlers. It keeps track of the file and size and makes some operations easier. It's meant for binary files rather than text.
· TorConnection: A wrapper for requests that proxies the connection through an already existing socks5 proxy to Tor. The socks5 proxy must be running externally (the script won't connect directly).
· PastebinAPI: A small and simple class that can upload data to pastebin using requests.
· MastoAPI: Wrapper for mastodon.py that makes certain tasks easier.
· EUtils: Miscelaneous functions.

# Requisites:

All of these scripts require Python version 3.9 or above.

· Debug: Requires *psutil* and *requests* (pip install psutil, requests)
· FileHandler: None
· TorConnection: Requires *requests* and *stem* (pip install requests[socks],stem)
· PastebinAPI: Requires *requests* (pip install requests)
· MastoAPI: Requires *mastodon.py* (pip install mastodon.py)
· EUtils: None

To install all requirements: * pip install psutil, requests, requests[socks], stem, mastodon.py *

## Installation

These modules are independent of each other, therefore there are no exports. You must copy the corresponding file into your
project folder and import them from your script. Example:

from Debug import Debug

## Authors and acknowledgment

I am the author. I acknowledge myself as a worthy programmer.

## License

This project is under the GPLv3 license.

## Disclaimer

I coded most of these modules very late at night, therefore I am not responsible for any screw ups I might have made that could potentially make you lose your job if you use my scripts.

## Project status

These are tools I use once in a while, therefore I might update them when needed.
When needed could be a couple of days, months, or years, depending on my needs.
