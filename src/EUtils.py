from math import floor
from random import shuffle

KIBMUL = 1
MIBMUL = 2
GIBMUL = 3
TIBMUL = 4
PIBMUL = 5
HIBMUL = 6

class EUtils:
    def bytesToMultiple(bytesval: int, unit: int = KIBMUL, round: bool = True, toiso = True) -> int:
        mul = 1024 if toiso else 1000
        res = bytesval
        for i in range(0,unit):
            res = res / mul
        if round:
            return floor(res)
        else:
            return res

    def humanReadable(bytesval: int, round: bool = True, toiso = True) -> int:
        mulval = 1024 if toiso else 1000
        if bytesval < mulval:
            return (bytesval, "B")
        if bytesval > mulval and bytesval < mulval ** 2:
            return (EUtils.bytesToMultiple(bytesval,KIBMUL,True,toiso), "KiB" if toiso else "KB")
        elif bytesval > mulval ** 2 and bytesval < mulval ** 3:
            return (EUtils.bytesToMultiple(bytesval,MIBMUL,True,toiso), "MiB" if toiso else "MB")
        elif bytesval > mulval ** 3 and bytesval < mulval ** 4:
            return (EUtils.bytesToMultiple(bytesval,GIBMUL,True,toiso), "GiB" if toiso else "GB")
        elif bytesval > mulval ** 4 and bytesval < mulval ** 5:
            return (EUtils.bytesToMultiple(bytesval,TIBMUL,True,toiso), "TiB" if toiso else "TB")
        elif bytesval > mulval ** 5 and bytesval < mulval ** 6:
            return (EUtils.bytesToMultiple(bytesval,PIBMUL,True,toiso), "PiB" if toiso else "PB")
        else:
            return (EUtils.bytesToMultiple(bytesval,HIBMUL,True,toiso), "HiB" if toiso else "HB")

    # Best sorting algorithm ever!!! It only takes many years to sort a 20 item array!
    # (Or maybe just a second, if you're lucky)
    def bogosort(data: list) -> list:
        ordered = False
        iterations = 0
        while not ordered:
            shuffle(data)
            iterations += 1
            for i in range(len(data)-1):
                if data[i] + 1 == data[i+1]:
                    if i == len(data)-2:
                        ordered = True
                else:
                    break
        return ordered
    # This is only a joke, but feel free to use it in production at work... if you're brave enough!