import requests

class Pastebin:
    def __init__(self, api_key, username, password):
        self.api_dev_key = api_key
        self.api_user_name = username
        self.api_user_password = password
        r = requests.post("https://pastebin.com/api/api_login.php", data={
            'api_dev_key': self.api_dev_key,
            'api_user_name': self.api_user_name,
            'api_user_password': self.api_user_password
        })
        if r.status_code != 200:
            print(f"Failed to log into pastebin ({r.status_code})\n{r.text}")
            return
        self.api_user_key = r.text
    def paste(self, paste_text: str, paste_name: str, expire_date: str = "1H", paste_format: str = "json") -> str:
        pasteData = {
            'api_option': 'paste',
            'api_dev_key': self.api_dev_key,
            'api_paste_code': paste_text,
            'api_paste_name': paste_name,
            'api_paste_expire_date': expire_date,
            'api_user_key': self.api_user_key,
            'api_paste_format': paste_format
        }
        r = requests.post("https://pastebin.com/api/api_post.php", data=pasteData)
        if r.status_code != 200:
            print(f"Failed to make a paste: {r.status_code}\n{r.text}")
            return None
        return r.text