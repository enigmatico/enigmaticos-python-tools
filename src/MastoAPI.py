from mastodon import Mastodon, StreamListener # pip install mastodon.py
import os
import datetime
import random
import hashlib
from Debug import Debug

class MastodonUserField:
    def __init__(self, field):
        self.name = field["name"]
        self.value = field["value"]
        self.verified_at = field["verified_at"] if "verified_at" in field else None

class MastodonNotification:
    def __init__(self, notif):
        self.id = notif["id"]
        self.type = notif["type"]
        self.created_at = notif["created_at"]
        self.account = MastodonUser(notif["account"])
        self.status = MastodonPost(notif["status"])

class MastodonUser:
    def __init__(self,user):
        self.id = user["id"]
        self.username = user["username"]
        self.acct = user["acct"]
        self.display_name = user["display_name"]
        self.discoverable = user["discoverable"]
        self.group = user["group"]
        self.locked = user["locked"]
        self.created_at = user["created_at"]
        self.following_count = user["following_count"]
        self.followers_count = user["followers_count"]
        self.statuses_count = user["statuses_count"]
        self.note = user["note"]
        self.url = user["url"]
        self.avatar = user["avatar"]
        self.header = user["header"]
        self.avatar_static = user["avatar_static"]
        self.source = user["source"] if "source" in user else None
        self.moved_to_account = user["moved_to_account"] if "moved_to_account" in user else None
        self.bot = user["bot"]
        self.fields = [MastodonUserField(f) for f in user["fields"]]
        self.emojis = [MastodonEmoji(em) for em in user["emojis"]]

class MastodonMention:
    def __init__(self, mention):
        self.url = mention["url"]
        self.username = mention["username"]
        self.acct = mention["acct"]
        self.id = mention["id"]

class MastodonScheduleParams:
    def __init__(self, params):
        self.text = params["text"]
        self.in_reply_to_id = params["in_reply_to_id"]
        self.media_ids = params["media_ids"]
        self.sensitive = params["sensitive"]
        self.visibility = params["visibility"]
        self.idempotency = params["idempotency"]
        self.scheduled_at = params["scheduled_at"]
        self.spoiler_text = params["spoiler_text"]
        self.application_id = params["application_id"]
        self.poll = params["poll"]

class MastodonSchedule:
    def __init__(self, sch):
        self.id = sch["id"]
        self.scheduled_at = sch["scheduled_at"]
        self.params = MastodonScheduleParams(sch["params"])
        self.media_attachments = sch["media_attachments"]

class MastodonPollOption:
    def __init__(self, option):
        self.title = option["title"]
        self.votes_count = option["votes_count"]

class MastodonPoll:
    def __init__(self, poll):
        self.id = poll["id"]
        self.expires_at = poll["expires_at"]
        self.expired = poll["expired"]
        self.multiple = poll["multiple"]
        self.votes_count = poll["votes_count"]
        self.voted = poll["voted"]
        self.options = [MastodonPollOption(option) for option in poll["options"]]
        self.emojis = [MastodonEmoji(e) for e in poll["emojis"]]
        self.own_votes = poll["own_votes"]

class MastodonConversation:
    def __init__(self, conversation):
        self.id = conversation["id"]
        self.unread = conversation["unread"]
        self.accounts = [MastodonUser(u) for u in conversation["accounts"]]
        self.last_status = conversation["last_status"]

class MastodonHashtagHistory:
    def __init__(self, hhistory):
        self.day = hhistory["day"]
        self.uses = hhistory["uses"]
        self.accounts = hhistory["accounts"]

class MastodonHashtag:
    def __init__(self, hashtag):
        self.name = hashtag["name"]
        self.url = hashtag["url"]
        if "history" in hashtag:
            self.history = [MastodonHashtagHistory(hi) for hi in hashtag["history"]]
        else:
            self.history = []

class MastodonEmoji:
    def __init__(self,emoji):
        self.shortcode = emoji["shortcode"]
        self.url = emoji["url"]
        self.static_url = emoji["static_url"]
        self.visible_in_picker = emoji["visible_in_picker"]
        if "category" in emoji:
            self.category = emoji["category"]
        else:
            self.category = None

class MastodonApplication:
    def __init__(self, application):
        self.name = application["name"]
        self.website = application["website"]
        self.vapid_key = application["vapid_key"]

class MastodonRelationship:
    def __init_(self, relat):
        self.id = relat["id"]
        self.following = relat["following"]
        self.followed_by = relat["followed_by"]
        self.blocking = relat["blocking"]
        self.muting = relat["muting"]
        self.muting_notifications = relat["muting_notifications"]
        self.requested = relat["requested"]
        self.domain_blocking = relat["domain_blocking"]
        self.showing_reblogs = relat["showing_reblogs"]
        self.endorsed = relat["endorsed"]

class MastodonFilter:
    def __init__(self, filter):
        self.id = filter["id"]
        self.phrase = filter["phrase"]
        self.context = filter["context"]
        self.expires_at = filter["expires_at"]
        self.irreversible = filter["irreversible"]
        self.whole_world = filter["whole_word"]

class MastodonNotification:
    def __init__(self, notif):
        self.id = notif["id"]
        self.type = notif["type"]
        self.created_at = notif["created_at"]
        self.account = MastodonUser(notif["account"])
        self.status = MastodonPost(notif["status"])

class MastodonContext:
    def __init__(self, con):
        self.ancestors = [MastodonPost(a) for a in con["ancestors"]]
        self.descendants = [MastodonPost(a) for a in con["descendants"]]

class MastodonList:
    def __init__(self, con):
        self.id = con['id']
        self.title = con['title']

class MastodonMediaMetadataImage:
    def __init__(self, mm):
        self.width = mm["width"] if "width" in mm else None
        self.height = mm["height"] if "height" in mm else None
        self.aspect = mm["aspect"] if "aspect" in mm else None
        self.size = mm["size"] if "size" in mm else None

class MastodonMediaMetadataVideo:
    def __init__(self, mm):
        self.width = mm["width"] if "width" in mm else None
        self.height = mm["height"] if "height" in mm else None
        self.frame_rate = mm["frame_rate"] if "frame_rate" in mm else None
        self.duration = mm["duration"] if "duration" in mm else None
        self.bitrate = mm["bitrate"] if "bitrate" in mm else None
        self.fps = mm["fps"] if "fps" in mm else None

class MastodonMediaFocus:
    def __init__(self, foc):
        self.x = foc["x"] if "x" in foc else None
        self.y = foc["y"] if "y" in foc else None

class MastodonMedia:
    def __init__(self, media):
        self.id = media["id"]
        self.type = media["type"]
        self.url = media["url"]
        self.remote_url = media["remote_url"]
        self.preview_url = media["preview_url"]
        self.text_url = media["text_url"]
        self.meta = []
        for meta in media["meta"]:
            if "size" in meta:
                self.meta.append(MastodonMediaMetadataImage(meta))
            elif "duration" in meta:
                self.meta.append(MastodonMediaMetadataVideo(meta))
            elif "x" in meta and "y" in meta:
                self.meta.append(MastodonMediaFocus(meta))
        self.blurhash = media["blurhash"]
        self.description = media["description"]

class MastodonCard:
    def __init__(self,card):
        self.url = card["url"]
        self.title = card["title"]
        self.description = card["description"]
        self.type = card["type"]
        self.image = card["image"] if "image" in card else None
        self.author_name = card["author_name"] if "author_name" in card else None
        self.author_url = card["author_url"] if "author_url" in card else None
        self.description = card["description"] if "description" in card else None
        self.width = card["width"] if "width" in card else None
        self.height = card["height"] if "height" in card else None
        self.html = card["html"] if "html" in card else None
        self.provider_name = card["provider_name"] if "provider_name" in card else None
        self.provider_url = card["provider_url"] if "provider_url" in card else None

class MastodonSearchResult:
    def __init__(self, sr):
        self.accounts = [MastodonUser(u) for u in sr["accounts"]] if "accounts" in sr else []
        self.hashtags = [MastodonHashtag(h) for h in sr["hashtags"]] if "hashtags" in sr else []
        self.statuses = [MastodonPost(p) for p in sr["statuses"]] if "statuses" in sr else []

class MastodonInstanceStats:
    def __init__(self, istat):
        self.user_count = istat["user_count"]
        self.status_count = istat["status_count"]
        self.domain_count = istat["domain_count"]

class MastodonInstance:
    def __init__(self, ins):
        self.description = ins["description"]
        self.short_description = ins["short_description"]
        self.email = ins["email"]
        self.title = ins["title"]
        self.uri = ins["uri"]
        self.version = ins["version"]
        self.urls = ins["urls"]
        self.stats =MastodonInstanceStats(ins["stats"])
        self.contact_account = MastodonUser(ins["contact_account"])
        self.languages = ins["languages"]
        self.registrations = ins["registrations"]
        self.approval_required = ins["approval_required"]

class MastodonActivity:
    def __init__(self, act):
        self.week = act["week"]
        self.logins = act["logins"]
        self.registrations = act["registrations"]
        self.statuses = act["statuses"]

class MastodonPost:
    def __init__(self, status):
        self.id = status["id"]
        self.uri = status["uri"]
        self.url = status["url"]
        self.account = MastodonUser(status["account"])
        self.in_reply_to_id = status["in_reply_to_id"]
        self.in_reply_to_account_id = status["in_reply_to_account_id"]
        self.reblog = status["reblog"]
        self.content = status["content"]
        self.created_at = status["created_at"]
        self.reblogs_count = status["reblogs_count"]
        self.favourites_count = status["favourites_count"]
        self.reblogged = status["reblogged"]
        self.favourited = status["favourited"]
        self.sensitive = status["sensitive"]
        self.spoilter_text = status["spoiler_text"]
        self.visibility = status["visibility"]
        self.mentions = [MastodonMention(mention) for mention in status["mentions"]]
        self.media_attachments = [MastodonMedia(m) for m in status["media_attachments"]]
        self.emojis = [MastodonEmoji(em) for em in status["emojis"]]
        self.tags = [MastodonHashtag(ha) for ha in status["tags"]]
        self.bookmarked = status["bookmarked"]
        if "application" in status:
            self.application = status["application"]
            #self.application = [MastodonApplication(ap) for ap in status["application"]] status["application"] is not None else None
        self.language = status["language"]
        self.muted = status["muted"]
        self.pinned = status["pinned"] if "pinned" in status else False
        self.replies_count = status["replies_count"]
        self.card = MastodonCard(status["card"]) if status["card"] is not None else None
        self.poll = [MastodonPoll(p) for p in status["poll"]] if status["poll"] is not None else None

class MastodonPreferences:
    def __init__(self,pref):
        self.default_visibility = pref["posting:default:visibility"]
        self.default_sensitive = pref["posting:default:sensitive"]
        self.default_language = pref["posting:default:language"]
        self.expand_media = pref["reading:expand:media"]
        self.expand_spoilers = pref["reading:expand:spoilers"]

class MastoAPI:
    def __init__(self, username: str, password: str, app_name: str, api_base_url: str,
    output_secret_file: str = "mastoapi.secret", output_usercred_file: str = "mastoapi_usercred.secret"):
        if not os.path.isfile(output_secret_file):
            Mastodon.create_app(app_name, api_base_url=api_base_url, to_file=output_secret_file)
        if not os.path.isfile(output_usercred_file):
            self.session = Mastodon(client_id=output_secret_file, api_base_url=api_base_url)
            self.session.log_in(username,password,to_file=output_usercred_file)
        else:
            self.session = Mastodon(access_token=output_usercred_file, api_base_url=api_base_url)
        self.instance = MastodonInstance(self.session.instance())
        self.account = MastodonUser(self.session.me())
        self.preferences = MastodonPreferences(self.session.preferences())
        self.filters = [MastodonFilter(f) for f in self.session.filters()]
        self.lists = [MastodonList(l) for l in self.session.lists()]
        self.bookmarks = [MastodonPost(b) for b in self.session.bookmarks()]
        self.instanceCustomEmoji = [MastodonEmoji(em) for em in self.session.custom_emojis()]

    def quickpost(self, str: str) -> MastodonPost:
        return MastodonPost(self.session.toot(str))

    def post(self, msg: str, visibility: str = "public", media_ids: list = None, in_reply_to_id = None) -> MastodonPost:
        return MastodonPost(self.session.status_post(status=msg, visibility=visibility, media_ids=media_ids, in_reply_to_id=in_reply_to_id))

    def upload(self, media: str or list or tuple) -> list:
        if isinstance(media, list) or isinstance(media, tuple):
            img_list = []
            for img in media:
                img_list.append(MastodonMedia(self.session.media_post(media_file=img)))
            return img_list
        elif isinstance(media, str):
            return [MastodonMedia(self.session.media_post(media_file=media))]
        else:
            return None

    def stream_user(self, listener_class: StreamListener, run_async: bool = False):
        return self.session.stream_user(listener_class, run_async=run_async)